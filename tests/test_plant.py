import pytest

@pytest.fixture(autouse=True)
def try_import():
    try:
        import plant
    except:
        assert False, "Plant class has not been implemented (Issue 1)"


def test_is_plant_name_correct():
    import plant
    name = "devil's ivy"
    p = plant.Plant(name)
    assert p.getName() == "Devil's Ivy", "The plant's name is not correct!"

def test_is_plant_size_correct():
    import plant
    p = plant.Plant("")
    assert p.getSize() == 0, "The plant's initial size is not correct!"

def test_is_plant_size_correct_after_grow():
    import plant
    p = plant.Plant("")
    for i in range(5):
        p.grow()
    assert p.getSize() == 5, "The plant's size is not correct!"
