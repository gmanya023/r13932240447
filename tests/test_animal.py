import animal

def test_is_lion_noise_correct():
    l = animal.Lion("lion1")
    assert "roar" in l.makeNoise().lower(), "Lion noise is incorrect"

def test_is_cow_noise_correct():
    c = animal.Cow("cow2")
    assert "moo" in c.makeNoise().lower(), "Cow noise is incorrect (Issue 2)"

def test_is_dog_noise_correct():
    d = animal.Dog("dog3")
    assert "woof" in d.makeNoise().lower(), "Dog noise is incorrect"
