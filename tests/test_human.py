import pytest

@pytest.fixture(autouse=True)
def try_import():
    try:
        import human
    except:
        assert False, "Human class has not been implemented (Issue 3)"


def test_is_baby_noise_correct():
    import human
    b = human.Baby("baby!", 0.7)
    assert "waa" in b.makeNoise().lower()

def test_is_adult_noise_correct():
    import human
    a = human.Adult("adult.", 56)
    assert "hello" in a.makeNoise().lower()

def test_is_human_correct():
    import human
    a = human.Adult("adult.", 56)
    assert a.getAge() == "I am 56 years old"
