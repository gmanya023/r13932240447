class Animal(object):
    def __init__(self, name):
        self.name = name

    def makeNoise(self):
        return ""

class Lion(Animal):
    def makeNoise(self):
        return "Roar!"

class Cow(Animal):
    def makeNoise(self):
        return "Meow?"

class Dog(Animal):
    def makeNoise(self):
        return "Woof!"
