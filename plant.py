class Plant(object):
    def __init__(self, name):
        self.name = name
        self.size = 0

    def getSize(self):
        return self.size

    def getName(self):
        return " ".join(word.capitalize() for word in self.name.split())

    def grow(self):
        self.size += 1
