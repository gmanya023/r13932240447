from animal import Animal

class Human(Animal):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def getAge(self):
        return f"I am {self.age} years old"

class Baby(Human):
    def makeNoise(self):
        return "waa!"

class Adult(Human):
    def makeNoise(self):
        return "Hello!"
